// const bodyParser = require("body-parser");
// const cors = require("cors");
require("dotenv").config();
const mongoose = require("mongoose");
const express = require("express");
const app = express();
const port = 3000;
const jwt = require("jsonwebtoken");

// app.use(bodyParser.json());
// app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//var distDir = __dirname + "/dist/";
//app.use(express.static(distDir));
app.use(function (err, req, res, next) {
  res.send("uncaught error!");
});

// MongoDB connection
// const userName = process.env.USER_NAME;
// const password = process.env.PASSWORD;
// const mongoURI =
//   "mongodb+srv://show-something:1234@cluster0.mongodb.net/inventory_items?retryWrites=true&w=majority";

const mongoURI = "mongodb://localhost:27017/show-something";
mongoose
  .connect(mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // serverSelectionTimeoutMS: 5000, // Increase the timeout to 5 seconds
    // socketTimeoutMS: 45000, // Set socket timeout to 45 seconds
    // useCreateIndex: true,
  })
  .then(() => console.log("MongoDB connected"))
  .catch((err) => console.log(err));

// Define a simple model for demonstration
const InventoryItemSchema = new mongoose.Schema({
  technicalId: { type: String, required: true, unique: true },
  model: { type: String, required: true },
  description: { type: String, required: true },
  addedAt: { type: Date, required: true },
});

const UserSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
});
const InventoryItem = mongoose.model("InventoryItem", InventoryItemSchema);
const User = mongoose.model("User", UserSchema);

// REQUESTS
// LOGIN| REGISTER
app.post("/ss/home/login", async (req, res) => {
  const loginInfo = { ...req.body, password: atob(req.body.password) };
  try {
    const foundUser = await User.findOne({
      email: loginInfo.email,
      password: loginInfo.password,
    });

    if (foundUser) {
      res.status(200).json(buildToken(foundUser._id));
    }

    if (!foundUser && loginInfo.isNewUser) {
      const user = await new User(loginInfo);
      await user.save();
      res.status(200).json(buildToken(foundUser._id));
    }

    if (!foundUser && !loginInfo.isNewUser) {
      res.status(401).json(false);
    }
  } catch {
    console.log("ERROR - Could not find user", loginInfo);
  }
  // console.log("All users => ", await User.find());
});

function buildToken(id) {
  const jwtKey = process.env.JWT_SECRET;

  const userToken = jwt.sign({ id: id.toString() }, jwtKey, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });

  return { token: userToken };
}

// CREATE | UPDATE
app.put("/ss/overview/save", async (req, res) => {
  const foundInventoryItem = await InventoryItem.findOne({
    technicalId: req.body.technicalId,
  });
  if (foundInventoryItem) {
    foundInventoryItem.model = req.body.model;
    foundInventoryItem.description = req.body.description;
    foundInventoryItem.addedAt = req.body.addedAt;
    await foundInventoryItem.save();
  } else {
    const newItem = new InventoryItem(req.body);
    await newItem.save();
  }

  console.log("Create|Update Inventory item with ", req.body);
  res.status(200).json(req.body);
});

// READ
app.get("/ss/overview/items", async (req, res) => {
  InventoryItem.find()
    .sort({ addedAt: -1 })
    .then((items) => res.status(200).json(items))
    .catch((err) => console.log("No inventory items found"));
  console.log("Fetch all inventory items...");
});

// DELETE
app.delete("/ss/overview/delete/:itemId", async (req, res) => {
  const inventoryItemId = req.params.itemId;
  InventoryItem.deleteOne({ technicalId: inventoryItemId })
    .then((item) => res.json({ success: true }))
    .catch((err) => res.status(404).json({ success: false }));
  console.log("Delete Inventory item ", inventoryItemId);
});

app.listen(port, () => {
  console.log("Server listening at port ", port);
});

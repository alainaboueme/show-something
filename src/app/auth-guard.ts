import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { inject } from '@angular/core';
import { AuthService } from './core/service/auth.service';

/* eslint-disable @typescript-eslint/no-unused-vars */
export const authGuard: CanActivateFn = (
  _next: ActivatedRouteSnapshot,
  _state: RouterStateSnapshot
) => {
  const authService = inject(AuthService);
  const router = inject(Router);
  const isLoggedIn = authService.isUserLoggedIn();

  if (!isLoggedIn) {
    router.navigate(['/home']);
    return false;
  }
  return true;
};

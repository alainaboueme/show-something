import { Pipe, PipeTransform } from '@angular/core';
import { translationCache } from '../../../../assets/translations/translation-cache';

@Pipe({
  name: 'translate',
  standalone: true
})
export class TranslatePipe implements PipeTransform {

  transform(key: string): string {
    // eslint-disable-next-line  @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const translation = translationCache[key] as string;

    return translation || key;
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationDialogComponent } from './confirmation-dialog.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { of, EMPTY } from 'rxjs';
import { SharedModule } from '../../shared.module';

describe('ConfirmationDialogComponent', () => {
  let component: ConfirmationDialogComponent;
  let fixture: ComponentFixture<ConfirmationDialogComponent>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<ConfirmationDialogComponent>>;

  beforeEach(async () => {
    const testData = {
      title: 'title',
      message: 'message',
    };
    dialogRef = jasmine.createSpyObj<MatDialogRef<ConfirmationDialogComponent>>(
      'dialogRef',
      ['close', 'afterClosed']
    );
    dialogRef.afterClosed.and.callFake(() => of(EMPTY));

    await TestBed.configureTestingModule({
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: testData },
        { provide: MatDialogRef, useValue: dialogRef },
      ],

      declarations: [ConfirmationDialogComponent],
      imports: [SharedModule],
    }).compileComponents();

    fixture = TestBed.createComponent(ConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should send the positive result as dialogResult', () => {
    // When
    component.sendResult(true);

    // Then
    expect(dialogRef.close).toHaveBeenCalledWith(true);
  });

  it('should send the negative result as dialogResult', () => {
    // When
    component.sendResult(false);

    // Then
    expect(dialogRef.close).toHaveBeenCalledWith(false);
  });
});

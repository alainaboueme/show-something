import { TestBed } from '@angular/core/testing';

import { NotificationService } from './notification.service';
import { TranslatePipe } from './pipe/translate.pipe';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('NotificationService', () => {
  let service: NotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TranslatePipe],
      imports: [NoopAnimationsModule, MatSnackBarModule],
    });
    service = TestBed.inject(NotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslatePipe } from './pipe/translate.pipe';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor(
    private readonly snackBar: MatSnackBar,
    private readonly translate: TranslatePipe
  ) {}

  displayPopUp(message: string, style: string): void {
    // style positive or negative
    this.snackBar.open(this.translate.transform(message), ' X', {
      panelClass: style,
      duration: 5000,
    });
  }
}

import { TestBed } from '@angular/core/testing';

import { DialogService } from './dialog.service';
import { Component } from '@angular/core';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { of, EMPTY } from 'rxjs';
import { TranslatePipe } from './pipe/translate.pipe';
import { ConfirmationDialogComponent } from './component/confirmation-dialog/confirmation-dialog.component';

@Component({
  template: ` <div>Dialog here</div>`,
})
class TestComponent {
  data = 'test-data';
}

describe('DialogServiceService', () => {
  let service: DialogService;
  let matDialog: jasmine.SpyObj<MatDialog>;
  let matDialogRef: jasmine.SpyObj<MatDialogRef<unknown>>;

  const testData = {
    data: {
      name: 'tester',
      age: 12,
    },
  };

  beforeEach(() => {
    matDialog = jasmine.createSpyObj<MatDialog>('matDialog', ['open']);
    matDialogRef = jasmine.createSpyObj<MatDialogRef<unknown>>('matDialogRef', [
      'close',
      'afterClosed',
    ]);

    matDialogRef.afterClosed.and.callFake(() => of(EMPTY));
    matDialog.open.and.returnValue(matDialogRef);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDialog,
          useValue: matDialog,
        },
      ],
      imports: [MatDialogModule, TranslatePipe],
    });
    service = TestBed.inject(DialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open a mat dialog', () => {
    // Given
    service.openMatDialogDialogThenDoOnClose(TestComponent, testData);

    // Then
    expect(matDialog.open).toHaveBeenCalledTimes(1);
    expect(matDialogRef.afterClosed).toHaveBeenCalledTimes(1);
  });

  it('should open the confirmation dialog', () => {
    // Given
    const title = 'Delete Item';
    const message = 'Confirm deletion?';
    const itemInfo = '12345';

    // When
    service.openConfirmationDialog(title, message, itemInfo);

    // Then
    expect(matDialog.open).toHaveBeenCalledTimes(1);
    expect(matDialog.open).toHaveBeenCalledOnceWith(
      ConfirmationDialogComponent,
      {
        width: '400px',
        position: { top: '150px' },
        data: {
          title,
          message,
          itemInfo,
        },
      }
    );
  });
});

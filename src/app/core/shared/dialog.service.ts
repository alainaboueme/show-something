import { Injectable, TemplateRef } from '@angular/core';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { firstValueFrom } from 'rxjs';
import { ConfirmationDialogComponent } from './component/confirmation-dialog/confirmation-dialog.component';

/* eslint-disable @typescript-eslint/no-explicit-any */
@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(private matDialog: MatDialog) {}

  openConfirmationDialog(
    title: string,
    message: string,
    itemInfo: string
  ): Promise<unknown> {
    const config = {
      data: {
        title,
        message,
        itemInfo,
      },
      width: '400px',
      position: {
        top: '150px',
      },
    } as MatDialogConfig;

    return this.openMatDialogDialogThenDoOnClose(
      ConfirmationDialogComponent,
      config
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  openMatDialogDialog(
    ref: TemplateRef<unknown> | any,
    data: unknown
  ): MatDialogRef<unknown, any> {
    const config = {
      data,
      position: {
        top: '150px',
      },
      width: '480px',
    } as MatDialogConfig;

    return this.matDialog.open(ref, config);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  openMatDialogDialogThenDoOnClose(
    ref: TemplateRef<unknown> | any,
    dialogConfig: MatDialogConfig
  ): Promise<unknown> {
    const config = {
      ...dialogConfig,
      position: {
        top: '150px',
      },
      width: dialogConfig.width ?? '480px',
    } as MatDialogConfig;
    const dialogRef = this.matDialog.open(ref, config);

    return firstValueFrom(dialogRef.afterClosed());
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { TranslatePipe } from '../shared/pipe/translate.pipe';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NotificationService } from '../shared/notification.service';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: HomeComponent,
      },
    ]),
    CommonModule,
    HttpClientModule,
    MatInputModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    TranslatePipe,
    MatButtonModule,
    MatSnackBarModule,
  ],
  providers: [TranslatePipe, NotificationService],
})
export class HomeModule {}

import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EMPTY, catchError, share, shareReplay, take } from 'rxjs';
import { NotificationService } from '../shared/notification.service';
import { AuthService } from '../service/auth.service';

// type UserInfo = {
//   email: string;
//   password: string;
//   isNewUser: boolean;
// };
type LoginResponse = {
  token: string;
};
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  homeFormGroup: FormGroup = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, Validators.required),
    isNewUser: new FormControl<boolean>(false),
  });
  constructor(
    private readonly router: Router,
    private readonly httpClient: HttpClient,
    private readonly notificationService: NotificationService,
    private authService: AuthService
  ) {}

  getControl(path: string): FormControl {
    return this.homeFormGroup.get(path) as FormControl;
  }

  navigateToOverview() {
    this.router.navigate(['/overview']);
  }

  doOnLoginIn(): void {
    const formValue = this.homeFormGroup.value;

    const valueToSend = {
      email: formValue.email,
      password: btoa(formValue.password),
      isNewUser: formValue.isNewUser,
    };
    // decode with atob
    this.httpClient
      .post<LoginResponse>(`/ss/home/login`, valueToSend)
      .pipe(
        take(1),
        share(),
        shareReplay(),
        catchError(() => {
          this.notificationService.displayPopUp(
            'app.home.login.failed',
            'negative'
          );
          // this.navigateToOverview(); // FALLBACK
          return EMPTY;
        })
      )
      .subscribe((response: LoginResponse) => {
        if (response) {
          const token = response.token;
          this.authService.logIn(token);
        }

        this.navigateToOverview();
      });
  }
}

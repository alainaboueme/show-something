import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CookieService {
  private ssKey = 'ss-cookie-consent-key';

  constructor() {}

  hasDecidedOnConsent(): boolean {
    return !!localStorage.getItem(this.ssKey);
  }

  hasGivenConsent(): boolean {
    const token = localStorage.getItem(this.ssKey);
    return token === 'true';
  }

  setConsent(consent: boolean): void {
    localStorage.setItem(this.ssKey, consent.toString());
  }

  resetConsent(): void {
    localStorage.removeItem(this.ssKey);
  }
}

import { Injectable, signal, WritableSignal } from '@angular/core';
import { CookieService } from './cookie.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  // isUserLoggedInSubject = new BehaviorSubject<boolean>(false);
  protected isUserLoggedInSubject = signal(this.hasValidToken());
  private ssToken = 'ss-token';

  constructor(private cookieService: CookieService) {}

  isUserLoggedIn: WritableSignal<boolean> = this.isUserLoggedInSubject;

  logOut(): void {
    this.isUserLoggedInSubject.set(false);
    if (this.cookieService.hasGivenConsent()) {
      localStorage.removeItem(this.ssToken);
    }
  }

  logIn(token: string): void {
    this.isUserLoggedInSubject.set(true);
    if (this.cookieService.hasGivenConsent()) {
      localStorage.setItem(this.ssToken, token);
    }
  }

  getToken(): string | null {
    return localStorage.getItem('ss-token');
  }

  hasValidToken(): boolean {
    const token = this.getToken();
    // REVIEW we will check the expire time
    return !!token;
  }
}

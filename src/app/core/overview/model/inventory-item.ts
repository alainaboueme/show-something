export interface InventoryItem {
  technicalId: string;
  model: string;
  description: string;
  addedAt: Date | null;
}

export function createEmptyInventoryItem(): InventoryItem {
  return {
    technicalId: crypto.randomUUID(),
    model: '',
    description: '',
    addedAt: null,
  };
}

export const FALLBACK_INVENTORY_ITEM: InventoryItem[] = [
  {
    ...createEmptyInventoryItem(),
    model: 'XW234',
    description: 'Model of old days',
    addedAt: new Date('2024/05/02'),
  },

  {
    ...createEmptyInventoryItem(),
    model: 'XW098',
    description: 'Model 2 of old days',
    addedAt: new Date('2024/05/12'),
  },
  {
    ...createEmptyInventoryItem(),
    model: 'XQM34',
    description: 'Model 3 of old days',
    addedAt: new Date('2024/05/20'),
  },
];

import { ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import {
  createEmptyInventoryItem,
  FALLBACK_INVENTORY_ITEM,
  InventoryItem,
} from './model/inventory-item';
import { DialogService } from '../shared/dialog.service';
import { MatDialogConfig } from '@angular/material/dialog';
import { InventoryItemFormComponent } from './component/inventory-item-form/inventory-item-form.component';
import { firstValueFrom, Observable, share } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../shared/notification.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Store } from '@ngrx/store';
import { OverviewState } from './overview.reducer';
import { DELETED, ADDED, Total } from './overviewAction';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrl: './overview.component.css',
})
export class OverviewComponent {
  inventoryItems: InventoryItem[] = [];
  private sort: MatSort | undefined;
  private paginator: MatPaginator | undefined;

  FAKE_DATA = FALLBACK_INVENTORY_ITEM;
  displayedColumns = ['model', 'description', 'addedAt', 'actions'];

  @ViewChild(MatSort) set matSort(element: MatSort) {
    this.sort = element;
    this.setSortAndPaginationToDataSource();
  }

  @ViewChild(MatPaginator) set matPaginator(element: MatPaginator) {
    this.paginator = element;
    this.setSortAndPaginationToDataSource();
  }
  dataSource: MatTableDataSource<InventoryItem> | undefined = undefined;
  stats$: Observable<OverviewState> | undefined = undefined;

  constructor(
    private readonly dialogService: DialogService,
    private readonly httpClient: HttpClient,
    private readonly notificationService: NotificationService,
    private readonly cdRef: ChangeDetectorRef,
    private store: Store<OverviewState>
  ) {
    this.fetchItems();
    this.stats$ = this.store;
  }

  confirmDeletion(inventoryItem: InventoryItem) {
    this.dialogService
      .openConfirmationDialog(
        'app.delete.item.text',
        'app.delete.message.text',
        inventoryItem.model
      )
      .then((response) => {
        if (response) {
          this.deleteItem(inventoryItem.technicalId)
            .then(() => {
              // upate the store and fetch new items
              this.store.dispatch({ type: DELETED });
              this.notificationService.displayPopUp(
                'app.overview.item.deleted',
                'positive'
              );
              this.fetchItems();
            })
            .catch(() =>
              this.notificationService.displayPopUp(
                'app.overview.item.not.deleted',
                'negative'
              )
            );
        }
      });
  }

  openInventoryItem(formData = createEmptyInventoryItem()) {
    const config = {
      data: { ...formData },
    } as MatDialogConfig;
    this.dialogService
      .openMatDialogDialogThenDoOnClose(InventoryItemFormComponent, config)
      .then((response) => {
        if (response) {
          const values = response as unknown as InventoryItem;
          this.saveItem(values)
            .then(() => {
              // upate the store and fetch new items
              this.store.dispatch({ type: ADDED });
              this.notificationService.displayPopUp(
                'app.overview.item.updated',
                'positive'
              );
              this.fetchItems();
            })
            .catch(() =>
              this.notificationService.displayPopUp(
                'app.overview.item.not.updated',
                'negative'
              )
            );
        }
      });
  }

  deleteItem(itemId: string) {
    return firstValueFrom(
      this.httpClient.delete<InventoryItem>(`/ss/overview/delete/${itemId}`)
    );
  }

  fetchItems(): void {
    // better subscribe to update after a change
    firstValueFrom(this.httpClient.get<InventoryItem[]>(`/ss/overview/items`))
      .then((response) => {
        // this.inventoryItems = response;
        this.inventoryItems = response as InventoryItem[];
        this.dataSource = new MatTableDataSource(this.inventoryItems);

        this.store.dispatch(new Total(response.length));
      })
      .catch(() => {
        // this.inventoryItems = this.FAKE_DATA;
      });
  }

  saveItem(item: InventoryItem) {
    return firstValueFrom(
      this.httpClient.put(`ss/overview/save`, item).pipe(share())
    );
  }

  private setSortAndPaginationToDataSource() {
    if (this.dataSource && this.sort && this.paginator) {
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }
  }
}

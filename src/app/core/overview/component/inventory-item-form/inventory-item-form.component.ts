import { Component, Inject } from '@angular/core';
import { InventoryItem } from '../../model/inventory-item';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-inventory-item-form',
  templateUrl: './inventory-item-form.component.html',
  styleUrl: './inventory-item-form.component.css',
})
export class InventoryItemFormComponent {
  inventoryItemFormValue: InventoryItem;
  inventoryItemFormGroup: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<InventoryItemFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: InventoryItem
  ) {
    this.inventoryItemFormValue = this.data;
    this.inventoryItemFormGroup = this.createInventoryFormGroup(this.data);
  }

  sendResult(): void {
    if (this.inventoryItemFormGroup.valid) {
      this.dialogRef.close(this.inventoryItemFormGroup.value);
    }
  }

  getControl(path: string): FormControl {
    return this.inventoryItemFormGroup.get(path) as FormControl;
  }

  isToday(): boolean {
    return this.getControl('addedAt').value == new Date();
  }

  createInventoryFormGroup(value: InventoryItem): FormGroup {
    return new FormGroup({
      technicalId: new FormControl(value.technicalId, [Validators.required]),
      model: new FormControl(value.model, Validators.required),
      description: new FormControl(value.description, Validators.required),
      addedAt: new FormControl(
        value.addedAt ?? new Date(),
        Validators.required
      ),
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryItemFormComponent } from './inventory-item-form.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FALLBACK_INVENTORY_ITEM } from '../../model/inventory-item';
import { EMPTY, of } from 'rxjs';
import { SharedModule } from '../../../shared/shared.module';

describe('InventoryItemFormComponent', () => {
  let component: InventoryItemFormComponent;
  let fixture: ComponentFixture<InventoryItemFormComponent>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<InventoryItemFormComponent>>;

  beforeEach(async () => {
    dialogRef = jasmine.createSpyObj<MatDialogRef<InventoryItemFormComponent>>(
      'dialogRef',
      ['close', 'afterClosed']
    );
    dialogRef.afterClosed.and.callFake(() => of(EMPTY));
    await TestBed.configureTestingModule({
      declarations: [InventoryItemFormComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: FALLBACK_INVENTORY_ITEM[0] },
        { provide: MatDialogRef, useValue: dialogRef },
      ],
      imports: [
        CommonModule,
        HttpClientModule,
        ReactiveFormsModule,
        SharedModule,
        NoopAnimationsModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(InventoryItemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

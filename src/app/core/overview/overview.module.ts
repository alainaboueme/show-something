import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverviewComponent } from './overview.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { InventoryItemFormComponent } from './component/inventory-item-form/inventory-item-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { StoreModule } from '@ngrx/store';
import { overviewReducer } from './overview.reducer';

@NgModule({
  declarations: [OverviewComponent, InventoryItemFormComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: OverviewComponent,
      },
    ]),
    SharedModule,
    MatSortModule,
    MatPaginatorModule,
    StoreModule.forRoot({ stats: overviewReducer }),
  ],
  exports: [],
})
export class OverviewModule {}

import { Action as RealAction } from '@ngrx/store';
import { ADDED, DELETED, Total, TOTAL } from './overviewAction';

export interface OverviewState {
  stats: OverviewStats;
}

export interface OverviewStats {
  total: number;
  deleted: number;
  added: number;
}

export function emptyStats(): OverviewStats {
  return {
    total: 0,
    deleted: 0,
    added: 0,
  };
}

// export type RealAction = OverviewStatsActions;

export function overviewReducer(
  report: OverviewStats = emptyStats(),
  action: RealAction
): OverviewStats {
  switch (action.type) {
    case ADDED:
      return {
        ...report,
        added: report.added + 1,
      };

    case DELETED:
      return {
        ...report,
        deleted: report.deleted + 1,
      };
    case TOTAL: {
      const act = action as unknown as Total;
      return {
        ...report,
        total: act.count ?? 0,
      };
    }
    default:
      return report;
  }
}

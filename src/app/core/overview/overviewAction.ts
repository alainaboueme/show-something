import { Action } from '@ngrx/store';

export const TOTAL = 'TOTAL';
export const ADDED = 'ADDED';
export const DELETED = 'DELETED';

export class Total implements Action {
  readonly type = TOTAL;
  constructor(public count: number) {}
}

export class Added implements Action {
  readonly type = ADDED;
}

export class Deleted implements Action {
  readonly type = DELETED;
}

export type OverviewStatsActions = Total | Added | Deleted;

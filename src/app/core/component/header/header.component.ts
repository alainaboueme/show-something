import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslatePipe } from '../../shared/pipe/translate.pipe';
import { CommonModule } from '@angular/common';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [TranslatePipe, CommonModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css',
})
export class HeaderComponent {
  constructor(
    private readonly router: Router,
    private authService: AuthService
  ) {
  }

  canSignout(): boolean {
    return this.router.url !== '/home' && this.isLoggedIn();
  }

  isLoggedIn(): boolean {
    return this.authService.isUserLoggedIn();
  }

  canGoToOverviewSignout(): boolean {
    return this.router.url !== '/overview' && this.isLoggedIn();
  }

  goToOverview() {
    this.router.navigate(['/overview']);
  }

  logOut(): void {
    this.authService.logOut();
    this.router.navigate(['/home']);
  }
}

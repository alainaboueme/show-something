import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CookieService } from '../../service/cookie.service';
import { CommonModule } from '@angular/common';
import { TranslatePipe } from '../../shared/pipe/translate.pipe';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-cookie',
  standalone: true,
  imports: [TranslatePipe, CommonModule, MatButtonModule],
  templateUrl: './cookie.component.html',
  styleUrls: ['./cookie.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CookieComponent {
  constructor(private cookieService: CookieService) {}

  hasToken(): boolean {
    return this.cookieService.hasDecidedOnConsent();
  }

  acceptCookie(): void {
    this.cookieService.setConsent(true);
  }

  declineCookie(): void {
    this.cookieService.setConsent(false);
  }
}

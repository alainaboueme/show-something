import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HeaderComponent } from './core/component/header/header.component';
import { CookieComponent } from './core/component/cookie/cookie.component';
import { TranslatePipe } from './core/shared/pipe/translate.pipe';
import { CookieService } from './core/service/cookie.service';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    HeaderComponent,
    CookieComponent,
    TranslatePipe,
    CommonModule,
    HttpClientModule,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'show-something';
  constructor(private cookieService: CookieService) {}

  resetCookieConsent(): void {
    this.cookieService.resetConsent();
  }
  canResetCookieConsent(): boolean {
    return this.cookieService.hasDecidedOnConsent();
  }
}
